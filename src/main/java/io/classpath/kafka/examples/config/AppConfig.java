package io.classpath.kafka.examples.config;

public class AppConfig {
    public final static String applicationID = "Producer-Demo";
    public final static String bootstrapServers = "159.89.168.63:9092";
    public final static String topicName = "test-producer";
    public final static int numEvents = 1000;
}
